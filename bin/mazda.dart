import 'car.dart';

class Mazda extends Car {
  Mazda(int cc, int hp, String color) : super(cc, hp, color);

  void brand() {
    print('Mazda Feel The Drive.');
  }

  void KODO() {
    print('ดีไซน์ความเป็นสปอร์ต ภายใต้แนวคิดแบบ KODO Design (Soul of motion)');
  }

  void iActivsense() {
    print(
        'ยกระดับความปลอดภัยอีกขั้นด้วยเทคโนโลยีสุดล้ำอย่าง I-Activsense ระบบป้องกันตัวเอง และคาดการณ์ล่วงหน้า');
  }

  void GVC() {
    print(
        'ชุดเทคโนโลยี G-VECTORING CONTROL (GVC) ช่วยให้ทุกการขับขี่มีความสมดุล');
  }

  void EPAS() {
    print(
        'EPAS ระบบพวงมาลัยไฟฟ้า ที่เป็นแบบแปรผันตามความเร็วเมื่อความเร็วต่ำ ก็สามารถควบคุมสบาย น้ำหนักเบา');
  }
}
