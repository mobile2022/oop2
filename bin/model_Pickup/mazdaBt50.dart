import '../feature.dart';
import '../interface/pickup.dart';
import '../mazda.dart';

class MazdaBt50 extends Mazda with Feature implements Pickup {
  MazdaBt50(String color) : super(1898, 150, color);

  void showModel() {
    print('Mazda BT-50');
  }

  @override
  void engineRZ4E() {
    print(
        'RZ4E-TC DOHC แถวเรียง 4 สูบ 16 วาล์ว VGS เทอร์โบและอินเตอร์คูลเลอร์');
  }

  @override
  void HLA() {
    // TODO: implement HLA
    super.HLA();
  }

  @override
  void HDC() {
    // TODO: implement HDC
    super.HDC();
  }

  @override
  void DSC() {
    // TODO: implement DSC
    super.DSC();
  }
}
