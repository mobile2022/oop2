import '../feature.dart';
import '../interface/cx_series.dart';
import '../mazda.dart';

class MazdaCx8 extends Mazda with Feature implements CxSeries {
  MazdaCx8(String color) : super(2191, 190, color);

  void showModel() {
    print('Mazda cx-8');
  }

  @override
  void engineSkyActiveD() {
    print(
        'Skyactiv-D 2.2 DOHC แถวเรียง 4 สูบ 16 วาล์วพร้อมระบบวาล์วไอเสียแปรผันอัจฉริยะ VVT และระบบเทอร์โบแปรผัน 2 ขั้น');
  }

  @override
  void ALH() {
    // TODO: implement ALH
    super.ALH();
  }

  @override
  void MRCC() {
    // TODO: implement MRCC
    super.MRCC();
  }

  @override
  void advanced_SBS() {
    // TODO: implement advanced_SBS
    super.advanced_SBS();
  }
}
