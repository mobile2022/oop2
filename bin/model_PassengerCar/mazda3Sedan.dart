import '../feature.dart';
import '../interface/passenger_car.dart';
import '../mazda.dart';

class Mazda3Sedan extends Mazda with Feature implements PassengerCar {
  Mazda3Sedan(String color) : super(1998, 165, color);

  void showModel() {
    print('Mazda 3 sedan');
  }

  @override
  void engineSkyActiveG() {
    print('Skyactiv-G 2.0 DOHC แถวเรียง 4 สูบ 16 วาล์ว พร้อมระบบวาล์ว');
  }

  @override
  void ALH() {
    // TODO: implement ALH
    super.ALH();
  }

  @override
  void CTS() {
    // TODO: implement CTS
    super.CTS();
  }

  @override
  void SBS_R() {
    // TODO: implement SBS_R
    super.SBS_R();
  }

  @override
  void advanced_SBS() {
    // TODO: implement advanced_SBS
    super.advanced_SBS();
  }
}
