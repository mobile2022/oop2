import '../feature.dart';
import '../interface/passenger_car.dart';
import '../mazda.dart';

class Mazda2Hatchback extends Mazda with Feature implements PassengerCar {
  Mazda2Hatchback(String color) : super(1299, 93, color);

  void showModel() {
    print('Mazda 2 hatchback');
  }

  @override
  void engineSkyActiveG() {
    print('Skyactiv-G 1.3 แถวเรียง 4 สูบ 16 วาล์ว');
  }

  @override
  void ALH() {
    // TODO: implement ALH
    super.ALH();
  }

  @override
  void CTS() {
    // TODO: implement CTS
    super.CTS();
  }

  @override
  void SBS_R() {
    // TODO: implement SBS_R
    super.SBS_R();
  }

  @override
  void ABSM() {
    // TODO: implement ABSM
  }
}
