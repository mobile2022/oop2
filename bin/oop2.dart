import 'package:oop2/oop2.dart' as oop2;

import 'mazda.dart';
import 'model_PassengerCar/mazda2Hatchback.dart';
import 'model_PassengerCar/mazda3Sedan.dart';
import 'model_CX-series/mazdaCx5.dart';
import 'model_CX-series/mazdaCx8.dart';
import 'model_Pickup/mazdaBt50.dart';

void main(List<String> arguments) {
  showcaseCar1();
  showcaseCar2();
  showcaseCar3();
  showcaseCar4();
  showcaseCar5();
}

void showcaseCar5() {
  var car5 = MazdaBt50('Gray');
  car5.showModel(); //Mazda BT-50 method
  car5.drive(); //Car method.
  car5.brand(); //Mazda method
  car5.KODO(); //Mazda method
  car5.iActivsense(); //Mazda method
  car5.GVC(); //Mazda method
  car5.EPAS(); //Mazda method
  car5.engineRZ4E(); //Method from interface
  car5.HLA(); //Mazda BT-50 method from mixin
  car5.HDC(); //Mazda BT-50 method from mixin
  car5.DSC(); //Mazda BT-50 method from mixin
  print("");
  print('---------------------------------------------');
  print("");
}

void showcaseCar4() {
  var car4 = MazdaCx8('Gold');
  car4.showModel(); //Mazda cx-8 method
  car4.drive(); //Car method.
  car4.brand(); //Mazda method
  car4.KODO(); //Mazda method
  car4.iActivsense(); //Mazda method
  car4.GVC(); //Mazda method
  car4.EPAS(); //Mazda method
  car4.engineSkyActiveD(); //Method from interface
  car4.ALH(); //Mazda cx-8 method from mixin
  car4.MRCC; //Mazda cx-8 method from mixin
  car4.advanced_SBS; //Mazda cx-8 method from mixin
  print("");
  print('---------------------------------------------');
  print("");
}

void showcaseCar3() {
  var car3 = MazdaCx5('Blue');
  car3.showModel(); //Mazda cx-5 method
  car3.drive(); //Car method.
  car3.brand(); //Mazda method
  car3.KODO(); //Mazda method
  car3.iActivsense(); //Mazda method
  car3.GVC(); //Mazda method
  car3.EPAS(); //Mazda method
  car3.engineSkyActiveD(); //Method from interface
  car3.ALH(); //Mazda cx-5 method from mixin
  car3.MRCC; //Mazda cx-5 method from mixin
  car3.advanced_SBS; //Mazda cx-5 method from mixin
  print("");
  print('---------------------------------------------');
  print("");
}

void showcaseCar2() {
  var car2 = Mazda3Sedan('Black');
  car2.showModel(); //Mazda 3 sedan method
  car2.drive(); //Car method.
  car2.brand(); //Mazda method
  car2.KODO(); //Mazda method
  car2.iActivsense(); //Mazda method
  car2.GVC(); //Mazda method
  car2.EPAS(); //Mazda method
  car2.engineSkyActiveG(); //Method from interface
  car2.ALH(); //Mazda 3 sedan method from mixin
  car2.CTS(); //Mazda 3 sedan method from mixin
  car2.SBS_R(); //Mazda 3 sedan method from mixin
  car2.advanced_SBS(); //Mazda 3 sedan method from mixin
  print("");
  print('---------------------------------------------');
  print("");
}

void showcaseCar1() {
  var car1 = Mazda2Hatchback('red');
  car1.showModel(); //Mazda 2 hatchback method
  car1.drive(); //Car method.
  car1.brand(); //Mazda method
  car1.KODO(); //Mazda method
  car1.iActivsense(); //Mazda method
  car1.GVC(); //Mazda method
  car1.EPAS(); //Mazda method
  car1.engineSkyActiveG(); //Method from interface
  car1.ALH(); //Mazda 2 hatchback method from mixin
  car1.CTS(); //Mazda 2 hatchback method from mixin
  car1.SBS_R(); //Mazda 2 hatchback method from mixin
  car1.ABSM(); //Mazda 2 hatchback method from mixin
  print("");
  print('---------------------------------------------');
  print("");
}
