abstract class Car {
  final int hp;
  final int cc;
  final String color;

  Car(this.cc, this.hp, this.color);

  void drive() {
    print('This car can drive');
  }

  get getHp => this.hp;
  get getCc => this.cc;
  get getColor => this.color;
}
