mixin Feature {
  void ALH() {
    print('Adaptive LED Headlamps ระบบไฟหน้า LED อัจฉริยะ');
  }

  void CTS() {
    print(
        'Cruising & Traffic Support ระบบควบคุมความเร็วและพวงมาลัยตามรถคันหน้า');
  }

  void SBS_R() {
    print(
        'Smart Brake Support Reverse ระบบช่วยเบรกและหยุดรถอัตโนมัติขณะถอยหลัง');
  }

  void advanced_SBS() {
    print(
        'Advanced Smart Brake Support ระบบเตือนการชนด้านหน้าและช่วยเบรกอัตโนมัติแบบ Advance');
  }

  void MRCC() {
    print(
        'MRCC With Stop & Go (MAZDA RADAR CRUISE CONTROL WITH STOP & GO) ระบบควบคุมความเร็วรถอัตโนมัติ แบบ Stop & Go สามารถปรับเพิ่ม-ลดความเร็วของรถแบบอัตโนมัติ');
  }

  void HLA() {
    print('Hill Launch Assist ระบบช่วยการออกตัวขณะอยู่บนทางลาดชัน');
  }

  void HDC() {
    print('Hill Descent Control ระบบควบคุมความเร็วขณะลงทางลาดชัน');
  }

  void DSC() {
    print('Dynamic Stability Control ระบบควบคุมเสถียรภาพ และการทรงตัวของรถ');
  }
}
